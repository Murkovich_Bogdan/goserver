package main

import (
	"../goserver/router"
)

func main() {
	r := router.New()

	r.Run()
}
