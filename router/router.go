package router

import (
	"github.com/gin-gonic/gin"
)

func New() *gin.Engine {
	r := gin.Default()
	r.LoadHTMLGlob("template/*")

	r.GET("/", indexHandler)
	return r
}
